#!/bin/sh
if [ $# -ne 2 ]; then
   echo "example usage: $0 \$ANDROID_ROOT ~/negitoro_binaries"
   exit 1
fi

JCROM_ANDROID_ROOT=$1
JCROM_NEGITORO_BIN=$2

mkdir -p ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/etc/permissions
mkdir -p ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/media/video
mkdir -p ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/framework
mkdir -p ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/GalleryGoogle
mkdir -p ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/GenieWidget
mkdir -p ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/LatinImeDictionaryPack
mkdir -p ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/GoogleLoginService
mkdir -p ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/FaceLock
mkdir -p ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/VideoEditorGoogle
mkdir -p ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/CalendarGoogle
mkdir -p ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/OneTimeInitializer
mkdir -p ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/SetupWizard
mkdir -p ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/talkback
mkdir -p ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/GoogleContactsSyncAdapter
mkdir -p ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/TagGoogle
mkdir -p ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/Music2
mkdir -p ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/ChromeBookmarksSyncAdapter
mkdir -p ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/GoogleFeedback
mkdir -p ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/MediaUploader
mkdir -p ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/Talk
mkdir -p ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/GoogleQuickSearchBox
mkdir -p ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/VoiceSearch
mkdir -p ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/Gmail
mkdir -p ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/Maps
mkdir -p ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/Thinkfree
mkdir -p ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/DeskClockGoogle
mkdir -p ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/Phonesky
mkdir -p ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/NetworkLocation
mkdir -p ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/PlusOne
mkdir -p ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/GoogleBackupTransport
mkdir -p ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/GooglePartnerSetup
mkdir -p ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/GoogleServicesFramework
mkdir -p ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/NfcGoogle
mkdir -p ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/YouTube
mkdir -p ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/GoogleTTS
mkdir -p ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/lib
mkdir -p ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/etc
mkdir -p ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/pittpatt/models/recognition/face.face.y0-y0-22-b-N
mkdir -p ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/right_eye-y0-yi45-p0-pi45-r0-ri20.2d_n2
mkdir -p ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/nose_base-y0-yi45-p0-pi45-rn7-ri20.2d_n2
mkdir -p ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/right_eye-y0-yi45-p0-pi45-rp7-ri20.2d_n2
mkdir -p ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/right_eye-y0-yi45-p0-pi45-rn7-ri20.2d_n2
mkdir -p ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/nose_base-y0-yi45-p0-pi45-r0-ri20.2d_n2
mkdir -p ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/left_eye-y0-yi45-p0-pi45-rn7-ri20.2d_n2
mkdir -p ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/nose_base-y0-yi45-p0-pi45-rp7-ri20.2d_n2
mkdir -p ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/left_eye-y0-yi45-p0-pi45-r0-ri20.2d_n2
mkdir -p ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/left_eye-y0-yi45-p0-pi45-rp7-ri20.2d_n2
mkdir -p ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/yaw_roll_face_detectors.3/head-y0-yi45-p0-pi45-rp30-ri30.5
mkdir -p ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/yaw_roll_face_detectors.3/head-y0-yi45-p0-pi45-rn30-ri30.5
mkdir -p ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/yaw_roll_face_detectors.3/head-y0-yi45-p0-pi45-r0-ri30.4a
mkdir -p ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/firmware
mkdir -p ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/lib/drm
mkdir -p ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/lib/hw

cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/etc/permissions/com.google.android.media.effects.xml ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/etc/permissions/com.google.android.media.effects.xml
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/etc/permissions/features.xml ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/etc/permissions/features.xml
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/etc/permissions/com.google.android.maps.xml ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/etc/permissions/com.google.android.maps.xml
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/etc/permissions/.gitignore ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/etc/permissions/.gitignore
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/etc/permissions/com.google.widevine.software.drm.xml ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/etc/permissions/com.google.widevine.software.drm.xml
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/media/bootanimation.zip ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/media/bootanimation.zip
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/media/PFFprec_600.emd ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/media/PFFprec_600.emd
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/media/LMprec_508.emd ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/media/LMprec_508.emd
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/media/video/Sunset.240p.mp4 ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/media/video/Sunset.240p.mp4
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/media/video/AndroidInSpace.240p.mp4 ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/media/video/AndroidInSpace.240p.mp4
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/media/video/AndroidInSpace.480p.mp4 ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/media/video/AndroidInSpace.480p.mp4
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/media/video/.gitignore ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/media/video/.gitignore
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/media/video/Sunset.480p.mp4 ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/media/video/Sunset.480p.mp4
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/media/video/Disco.240p.mp4 ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/media/video/Disco.240p.mp4
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/media/video/Disco.480p.mp4 ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/media/video/Disco.480p.mp4
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/framework/.gitignore ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/framework/.gitignore
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/framework/com.google.android.media.effects.jar ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/framework/com.google.android.media.effects.jar
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/framework/com.google.android.maps.jar ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/framework/com.google.android.maps.jar
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/framework/com.google.widevine.software.drm.jar ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/framework/com.google.widevine.software.drm.jar
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/GalleryGoogle/GalleryGoogle.apk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/GalleryGoogle/GalleryGoogle.apk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/GalleryGoogle/Android.mk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/GalleryGoogle/Android.mk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/GenieWidget/Android.mk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/GenieWidget/Android.mk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/GenieWidget/GenieWidget.apk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/GenieWidget/GenieWidget.apk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/LatinImeDictionaryPack/LatinImeDictionaryPack.apk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/LatinImeDictionaryPack/LatinImeDictionaryPack.apk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/LatinImeDictionaryPack/Android.mk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/LatinImeDictionaryPack/Android.mk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/GoogleLoginService/Android.mk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/GoogleLoginService/Android.mk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/GoogleLoginService/GoogleLoginService.apk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/GoogleLoginService/GoogleLoginService.apk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/FaceLock/Android.mk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/FaceLock/Android.mk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/FaceLock/FaceLock.apk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/FaceLock/FaceLock.apk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/VideoEditorGoogle/Android.mk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/VideoEditorGoogle/Android.mk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/VideoEditorGoogle/VideoEditorGoogle.apk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/VideoEditorGoogle/VideoEditorGoogle.apk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/CalendarGoogle/Android.mk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/CalendarGoogle/Android.mk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/CalendarGoogle/CalendarGoogle.apk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/CalendarGoogle/CalendarGoogle.apk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/OneTimeInitializer/Android.mk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/OneTimeInitializer/Android.mk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/OneTimeInitializer/OneTimeInitializer.apk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/OneTimeInitializer/OneTimeInitializer.apk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/SetupWizard/Android.mk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/SetupWizard/Android.mk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/SetupWizard/SetupWizard.apk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/SetupWizard/SetupWizard.apk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/talkback/Android.mk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/talkback/Android.mk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/talkback/talkback.apk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/talkback/talkback.apk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/GoogleContactsSyncAdapter/Android.mk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/GoogleContactsSyncAdapter/Android.mk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/GoogleContactsSyncAdapter/GoogleContactsSyncAdapter.apk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/GoogleContactsSyncAdapter/GoogleContactsSyncAdapter.apk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/TagGoogle/Android.mk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/TagGoogle/Android.mk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/TagGoogle/TagGoogle.apk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/TagGoogle/TagGoogle.apk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/Music2/Android.mk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/Music2/Android.mk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/Music2/Music2.apk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/Music2/Music2.apk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/ChromeBookmarksSyncAdapter/Android.mk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/ChromeBookmarksSyncAdapter/Android.mk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/ChromeBookmarksSyncAdapter/ChromeBookmarksSyncAdapter.apk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/ChromeBookmarksSyncAdapter/ChromeBookmarksSyncAdapter.apk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/GoogleFeedback/Android.mk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/GoogleFeedback/Android.mk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/GoogleFeedback/GoogleFeedback.apk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/GoogleFeedback/GoogleFeedback.apk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/MediaUploader/Android.mk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/MediaUploader/Android.mk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/MediaUploader/MediaUploader.apk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/MediaUploader/MediaUploader.apk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/Talk/Android.mk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/Talk/Android.mk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/Talk/Talk.apk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/Talk/Talk.apk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/GoogleQuickSearchBox/Android.mk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/GoogleQuickSearchBox/Android.mk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/GoogleQuickSearchBox/GoogleQuickSearchBox.apk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/GoogleQuickSearchBox/GoogleQuickSearchBox.apk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/VoiceSearch/Android.mk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/VoiceSearch/Android.mk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/VoiceSearch/VoiceSearch.apk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/VoiceSearch/VoiceSearch.apk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/Gmail/Gmail.apk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/Gmail/Gmail.apk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/Gmail/Android.mk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/Gmail/Android.mk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/Maps/Android.mk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/Maps/Android.mk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/Maps/Maps.apk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/Maps/Maps.apk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/Thinkfree/Thinkfree.apk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/Thinkfree/Thinkfree.apk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/Thinkfree/Android.mk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/Thinkfree/Android.mk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/DeskClockGoogle/Android.mk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/DeskClockGoogle/Android.mk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/DeskClockGoogle/DeskClockGoogle.apk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/DeskClockGoogle/DeskClockGoogle.apk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/Phonesky/Phonesky.apk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/Phonesky/Phonesky.apk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/Phonesky/Android.mk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/Phonesky/Android.mk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/NetworkLocation/Android.mk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/NetworkLocation/Android.mk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/NetworkLocation/NetworkLocation.apk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/NetworkLocation/NetworkLocation.apk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/PlusOne/Android.mk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/PlusOne/Android.mk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/PlusOne/PlusOne.apk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/PlusOne/PlusOne.apk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/GoogleBackupTransport/GoogleBackupTransport.apk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/GoogleBackupTransport/GoogleBackupTransport.apk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/GoogleBackupTransport/Android.mk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/GoogleBackupTransport/Android.mk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/GooglePartnerSetup/Android.mk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/GooglePartnerSetup/Android.mk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/GooglePartnerSetup/GooglePartnerSetup.apk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/GooglePartnerSetup/GooglePartnerSetup.apk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/GoogleServicesFramework/Android.mk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/GoogleServicesFramework/Android.mk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/GoogleServicesFramework/GoogleServicesFramework.apk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/GoogleServicesFramework/GoogleServicesFramework.apk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/NfcGoogle/NfcGoogle.apk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/NfcGoogle/NfcGoogle.apk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/NfcGoogle/Android.mk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/NfcGoogle/Android.mk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/YouTube/Android.mk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/YouTube/Android.mk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/YouTube/YouTube.apk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/YouTube/YouTube.apk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/GoogleTTS/GoogleTTS.apk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/GoogleTTS/GoogleTTS.apk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/app/GoogleTTS/Android.mk ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/app/GoogleTTS/Android.mk
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/lib/libgcomm_jni.so ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/lib/libgcomm_jni.so
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/lib/libext2fs.so ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/lib/libext2fs.so
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/lib/libext2_uuid.so ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/lib/libext2_uuid.so
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/lib/libvoicesearch.so ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/lib/libvoicesearch.so
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/lib/libnfc_jni.so ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/lib/libnfc_jni.so
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/lib/libext2_profile.so ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/lib/libext2_profile.so
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/lib/.gitignore ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/lib/.gitignore
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/lib/libfilterpack_facedetect.so ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/lib/libfilterpack_facedetect.so
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/lib/libflint_engine_jni_api.so ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/lib/libflint_engine_jni_api.so
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/lib/libearthmobile.so ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/lib/libearthmobile.so
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/lib/libnfc.so ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/lib/libnfc.so
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/lib/libfrsdk.so ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/lib/libfrsdk.so
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/lib/libpicowrapper.so ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/lib/libpicowrapper.so
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/lib/libspeexwrapper.so ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/lib/libspeexwrapper.so
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/lib/libext2_blkid.so ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/lib/libext2_blkid.so
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/lib/libfacelock_jni.so ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/lib/libfacelock_jni.so
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/lib/libvideochat_stabilize.so ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/lib/libvideochat_stabilize.so
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/lib/libmicrobes_jni.so ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/lib/libmicrobes_jni.so
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/lib/libext2_e2p.so ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/lib/libext2_e2p.so
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/lib/libext2_com_err.so ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/lib/libext2_com_err.so
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/lib/libvideochat_jni.so ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/lib/libvideochat_jni.so
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/vendor/etc/fallback_fonts.xml ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/etc/fallback_fonts.xml
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/vendor/etc/.gitignore ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/etc/.gitignore
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/vendor/etc/sirfgps.conf ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/etc/sirfgps.conf
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/vendor/etc/smc_normal_world_android_cfg.ini ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/etc/smc_normal_world_android_cfg.ini
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/vendor/pittpatt/models/recognition/face.face.y0-y0-22-b-N/.gitignore ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/pittpatt/models/recognition/face.face.y0-y0-22-b-N/.gitignore
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/vendor/pittpatt/models/recognition/face.face.y0-y0-22-b-N/full_model.bin ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/pittpatt/models/recognition/face.face.y0-y0-22-b-N/full_model.bin
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/right_eye-y0-yi45-p0-pi45-r0-ri20.2d_n2/.gitignore ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/right_eye-y0-yi45-p0-pi45-r0-ri20.2d_n2/.gitignore
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/right_eye-y0-yi45-p0-pi45-r0-ri20.2d_n2/full_model.bin ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/right_eye-y0-yi45-p0-pi45-r0-ri20.2d_n2/full_model.bin
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/nose_base-y0-yi45-p0-pi45-rn7-ri20.2d_n2/.gitignore ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/nose_base-y0-yi45-p0-pi45-rn7-ri20.2d_n2/.gitignore
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/nose_base-y0-yi45-p0-pi45-rn7-ri20.2d_n2/full_model.bin ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/nose_base-y0-yi45-p0-pi45-rn7-ri20.2d_n2/full_model.bin
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/right_eye-y0-yi45-p0-pi45-rp7-ri20.2d_n2/.gitignore ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/right_eye-y0-yi45-p0-pi45-rp7-ri20.2d_n2/.gitignore
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/right_eye-y0-yi45-p0-pi45-rp7-ri20.2d_n2/full_model.bin ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/right_eye-y0-yi45-p0-pi45-rp7-ri20.2d_n2/full_model.bin
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/right_eye-y0-yi45-p0-pi45-rn7-ri20.2d_n2/.gitignore ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/right_eye-y0-yi45-p0-pi45-rn7-ri20.2d_n2/.gitignore
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/right_eye-y0-yi45-p0-pi45-rn7-ri20.2d_n2/full_model.bin ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/right_eye-y0-yi45-p0-pi45-rn7-ri20.2d_n2/full_model.bin
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/nose_base-y0-yi45-p0-pi45-r0-ri20.2d_n2/.gitignore ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/nose_base-y0-yi45-p0-pi45-r0-ri20.2d_n2/.gitignore
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/nose_base-y0-yi45-p0-pi45-r0-ri20.2d_n2/full_model.bin ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/nose_base-y0-yi45-p0-pi45-r0-ri20.2d_n2/full_model.bin
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/left_eye-y0-yi45-p0-pi45-rn7-ri20.2d_n2/.gitignore ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/left_eye-y0-yi45-p0-pi45-rn7-ri20.2d_n2/.gitignore
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/left_eye-y0-yi45-p0-pi45-rn7-ri20.2d_n2/full_model.bin ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/left_eye-y0-yi45-p0-pi45-rn7-ri20.2d_n2/full_model.bin
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/nose_base-y0-yi45-p0-pi45-rp7-ri20.2d_n2/.gitignore ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/nose_base-y0-yi45-p0-pi45-rp7-ri20.2d_n2/.gitignore
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/nose_base-y0-yi45-p0-pi45-rp7-ri20.2d_n2/full_model.bin ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/nose_base-y0-yi45-p0-pi45-rp7-ri20.2d_n2/full_model.bin
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/left_eye-y0-yi45-p0-pi45-r0-ri20.2d_n2/.gitignore ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/left_eye-y0-yi45-p0-pi45-r0-ri20.2d_n2/.gitignore
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/left_eye-y0-yi45-p0-pi45-r0-ri20.2d_n2/full_model.bin ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/left_eye-y0-yi45-p0-pi45-r0-ri20.2d_n2/full_model.bin
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/left_eye-y0-yi45-p0-pi45-rp7-ri20.2d_n2/.gitignore ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/left_eye-y0-yi45-p0-pi45-rp7-ri20.2d_n2/.gitignore
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/left_eye-y0-yi45-p0-pi45-rp7-ri20.2d_n2/full_model.bin ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/left_eye-y0-yi45-p0-pi45-rp7-ri20.2d_n2/full_model.bin
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/yaw_roll_face_detectors.3/head-y0-yi45-p0-pi45-rp30-ri30.5/.gitignore ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/yaw_roll_face_detectors.3/head-y0-yi45-p0-pi45-rp30-ri30.5/.gitignore
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/yaw_roll_face_detectors.3/head-y0-yi45-p0-pi45-rp30-ri30.5/full_model.bin ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/yaw_roll_face_detectors.3/head-y0-yi45-p0-pi45-rp30-ri30.5/full_model.bin
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/yaw_roll_face_detectors.3/head-y0-yi45-p0-pi45-rn30-ri30.5/.gitignore ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/yaw_roll_face_detectors.3/head-y0-yi45-p0-pi45-rn30-ri30.5/.gitignore
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/yaw_roll_face_detectors.3/head-y0-yi45-p0-pi45-rn30-ri30.5/full_model.bin ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/yaw_roll_face_detectors.3/head-y0-yi45-p0-pi45-rn30-ri30.5/full_model.bin
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/yaw_roll_face_detectors.3/head-y0-yi45-p0-pi45-r0-ri30.4a/.gitignore ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/yaw_roll_face_detectors.3/head-y0-yi45-p0-pi45-r0-ri30.4a/.gitignore
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/yaw_roll_face_detectors.3/head-y0-yi45-p0-pi45-r0-ri30.4a/full_model.bin ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/pittpatt/models/detection/yaw_roll_face_detectors.3/head-y0-yi45-p0-pi45-r0-ri30.4a/full_model.bin
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/vendor/firmware/smc_pa_wvdrm.ift ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/firmware/smc_pa_wvdrm.ift
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/vendor/firmware/libpn544_fw.so ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/firmware/libpn544_fw.so
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/vendor/firmware/.gitignore ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/firmware/.gitignore
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/vendor/firmware/ducati-m3.bin ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/firmware/ducati-m3.bin
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/vendor/firmware/bcm4330.hcd ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/firmware/bcm4330.hcd
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/vendor/lib/libWVStreamControlAPI_L1.so ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/lib/libWVStreamControlAPI_L1.so
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/vendor/lib/drm/libdrmwvmplugin.so ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/lib/drm/libdrmwvmplugin.so
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/vendor/lib/drm/.gitignore ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/lib/drm/.gitignore
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/vendor/lib/libwvdrm_L1.so ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/lib/libwvdrm_L1.so
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/vendor/lib/libinvensense_mpl.so ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/lib/libinvensense_mpl.so
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/vendor/lib/hw/.gitignore ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/lib/hw/.gitignore
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/vendor/lib/hw/gps.omap4.so ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/lib/hw/gps.omap4.so
cp -a ${JCROM_ANDROID_ROOT}/vendor/sola/negitoro/proprietary/system/vendor/lib/libwvm.so ${JCROM_NEGITORO_BIN}/sola/negitoro/proprietary/system/vendor/lib/libwvm.so

